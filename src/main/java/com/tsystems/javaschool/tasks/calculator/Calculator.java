package com.tsystems.javaschool.tasks.calculator;

import java.util.*;

public class Calculator {
    private final String OPERATORS = "/*-+";
    private final String DELIMITERS = OPERATORS + "()";

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        ArrayList<String> expression_elements = rpn(statement);
        if (expression_elements == null || expression_elements.isEmpty())
            return null;
        ArrayDeque<Double> calc_stack = new ArrayDeque();
        for (String el: expression_elements)
        {
            if (isNumber(el))
                calc_stack.addLast(Double.valueOf(el));
            else if (isOperator(el)) {
                Double result;
                try {
                    if (el.equals("+"))
                        result = calc_stack.pollLast() + calc_stack.pollLast();
                    else if (el.equals("-"))
                        result = -calc_stack.pollLast() + calc_stack.pollLast();
                    else if (el.equals("*"))
                        result = calc_stack.pollLast() * calc_stack.pollLast();
                    else if (el.equals("/")) {
                        Double a, b;
                        b = calc_stack.pollLast();
                        a = calc_stack.pollLast();
                        result = a / b;
                    } else
                        result = -calc_stack.pollLast();
                    if (result.isInfinite() || result.isNaN())
                        return null;
                    calc_stack.addLast(result);
                }
                catch (NullPointerException e) {
                    return null;
                }
            }
        }
        Double answer = calc_stack.pollLast();
        answer = (double)Math.round(answer * 10000) / 10000;
        return answer.toString().replaceAll("\\.0+$", "");
    }

    private boolean isOperator(String s) {
        return OPERATORS.contains(s) || s.equals("u-");
    }

    private boolean isDelimiter(String s) {
        return DELIMITERS.contains(s);
    }

    private boolean isNumber(String s) {
        try {
            Double num = Double.valueOf(s);
            return true;
        }
        catch (NumberFormatException e) {
            return false;
        }
    }

    private int checkPriority(String op) {
        if (op.equals("u-"))
            return 2;
        else if (op.equals("*") || op.equals("/"))
            return 1;
        else
            return 0;
    }

    // reverse Polish notation
    private ArrayList<String> rpn(String s) {
        if (s == null || s.isEmpty())
            return null;
        // remove all whitespaces from expression
        s = s.replaceAll(" ", "");
        StringTokenizer st = new StringTokenizer(s, DELIMITERS, true);
        ArrayDeque<String> stack = new ArrayDeque<>();
        String cur_symbol = "";
        String prev_symbol = "";
        ArrayList<String> output = new ArrayList<>();
        while (st.hasMoreTokens()) {
            cur_symbol = st.nextToken();
            if (!st.hasMoreTokens() && isOperator(cur_symbol))
                return null;
            if (isOperator(cur_symbol) && isOperator(prev_symbol))
                return null;
            if (!stack.isEmpty()) {
                if (isNumber(cur_symbol))
                    output.add(cur_symbol);
                else if (isDelimiter(cur_symbol)) {
                    if (isOperator(cur_symbol)) {
                        // unary minus
                        if (cur_symbol.equals("-") && (prev_symbol.equals("")) ||
                                isDelimiter(prev_symbol) && !prev_symbol.equals(")"))
                            stack.addLast("u-");
                        else if ((!stack.getLast().equals("(")) &&
                                (checkPriority(cur_symbol) <= checkPriority(stack.getLast()))) {
                            output.add(stack.pollLast());
                            stack.addLast(cur_symbol);
                        }
                        else
                            stack.addLast(cur_symbol);
                    }
                    else if (cur_symbol.equals("(")){
                        stack.addLast(cur_symbol);
                    }
                    else {
                        while (!(stack.getLast().equals("("))) {
                            output.add(stack.pollLast());
                            if (stack.isEmpty())
                                return null;
                        }
                        // remove opening bracket after extracting
                        // all operators inside parentheses
                        if (!stack.isEmpty() && stack.getLast().equals("("))
                            stack.pollLast();
                        else
                        	// parentheses are not agreed
                            return null;
                    }
                }
            }
            else {
                if (isNumber(cur_symbol))
                    output.add(cur_symbol);
                else if (isDelimiter(cur_symbol) && !cur_symbol.equals(")")) {
                    // unary minus
                    if (cur_symbol.equals("-") && (prev_symbol.equals("")) ||
                            cur_symbol.equals("-") &&
                                    isDelimiter(prev_symbol) && !prev_symbol.equals(")")) {
                        cur_symbol = "u-";
                    }
                    stack.addLast(cur_symbol);
                }
                else {
                	// expression starts with closing bracket or other
                    // illegal symbol
                    // System.out.println("Illegal symbol in expression");
                    return null;
                }
            }
            prev_symbol = cur_symbol;
        }
        while (!stack.isEmpty())
            if (!stack.getLast().equals("("))
                output.add(stack.pollLast());
            else
                return null;
        return output;
    }

}
