package com.tsystems.javaschool.tasks.pyramid;

import java.util.*;

public class PyramidBuilder {
    private int checkLength(List<Integer> inputNumbers) {
        if (inputNumbers.isEmpty())
            throw new CannotBuildPyramidException();
        int len = inputNumbers.size();
        // check if the list size is triangular number
        int a = 1;
        int b = 1;
        int c = -2 * len;
        double d, x1, x2;
        d = b * b - 4 * a * c;
        if (d > 0) {
            x1 = (-b - Math.sqrt(d) / (2 * a));
            x2 = (-b + Math.sqrt(d)) / (2 * a);
            if ((int)x1 == x1)
                return (int)x1;
            else if ((int)x2 == x2)
                return (int)x2;
        }
        return -1;
    }
    
    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        int rows = checkLength(inputNumbers);
        if (rows == -1)
            throw new CannotBuildPyramidException();
        int columns = 2 * rows - 1;
        try {
            Collections.sort(inputNumbers);
        }
        catch (NullPointerException e) {
            throw new CannotBuildPyramidException();
        }
        int result[][] = new int[rows][columns];
        int num;
        int index = 0;
        int middle = (columns - 1) / 2;
        for (int row = 0; row < rows; row++) {
            for (int k = -row; k <= row; k += 2) {
                num = inputNumbers.get(index);
                result[row][k + middle] = num;
                index++;
            }
        }
        return result;
    }


}
