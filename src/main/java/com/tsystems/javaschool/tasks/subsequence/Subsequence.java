package com.tsystems.javaschool.tasks.subsequence;

import java.util.*;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        try {
            if (y.containsAll(x)) {
                int prevIndex = -1;
                int lastIndex;
                int curIndex;
                for (Object el: x)  {
                    curIndex = y.indexOf(el);
                    lastIndex = y.lastIndexOf(el);
                    if (curIndex <= prevIndex && lastIndex <= prevIndex)
                        return false;
                    prevIndex = curIndex;
                }
                return true;
            }
            return false;
        }
        catch (NullPointerException e) {
            throw new IllegalArgumentException();
        }
    }
}
